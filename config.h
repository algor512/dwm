/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int swallowfloating    = 0;        /* 1 means swallow floating windows by default */
static const int showbar            = 1;        /* 0 means no bar */
static const int status_right_pad   = 5;        /* status right padding */
static const int topbar             = 1;        /* 0 means bottom bar */
static const int resize_step        = 25;       /* default resize step */
static const char *fonts[]          = { "Inconsolata:size=13" };
static const char dmenufont[]       = "Inconsolata:size=13";
static const char col_gray1[]       = "#222222";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm]     = { "#ffffff", "#000000", col_gray2 },
	[SchemeSel]      = { "#ffffff", col_cyan,  col_cyan  },
	[SchemeSep]      = { "#404040", "#000000", col_gray2 },
	[SchemeInactive] = { "#969696", "#000000", col_gray2 },
	[SchemeGood]     = { "#00ff00", "#000000", col_gray2 },
	[SchemeBad]      = { "#ff0000", "#000000", col_gray2 },
	[SchemeUrgent]   = { "#ffffff", "#ff0000", col_gray2 },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class     instance  title           tags mask  isfloating  isterminal  noswallow  monitor xkb_layout xkb_fix */
	{ "st",      NULL,     NULL,           0,         0,          1,          -1,        -1,     0,         0 },
	{ "Emacs",   NULL,     NULL,           0,         0,          0,          -1,        -1,     0,         1 },
	{ NULL,      NULL,     "Event Tester", 0,         1,          0,           1,        -1,     0,         1 }, /* xev */
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

/* xkb frontend */
static const char *xkb_layouts [] = {
    "en",
    "ru",
};

static const Layout layouts[] = {
	/* отображение  функция управления окнами */
	{ "[M]",        monocle },    /* default */
	{ "[T]",        tile },
	{ NULL,         NULL },
};

static void hjkl_func(const Arg *arg);
static void float_and_center(const Arg *arg);
static void myresize(const Arg *arg);
static void mymove(Client *c, int step, char direction);
static void change_cursor_position(Client *c, int oldx, int oldy, int oldw, int oldh);

/* key definition */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenu_run_cmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-l", "25", NULL };
static const char *dmenu_mount_cmd[] = { "dmenu_mount", "-m", dmenumon, "-fn", dmenufont, "-l", "25", NULL };
static const char *emacs_cmd[] = { "emacs", NULL };
static const char *term_cmd[]  = { "st", NULL };
static const char *flameshot_cmd[]  = { "flameshot", "gui", NULL };

static const Arg volume_raise_cmd = SHCMD("dwm_actions.sh inc-volume");
static const Arg volume_lower_cmd = SHCMD("dwm_actions.sh dec-volume");
static const Arg brightness_up_cmd = SHCMD("dwm_actions.sh inc-bright");
static const Arg brightness_dowm_cmd = SHCMD("dwm_actions.sh dec-bright");


static Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY,                       XK_r,      spawn,          {.v = dmenu_run_cmd } },
	{ MODKEY,                       XK_m,      spawn,          {.v = dmenu_mount_cmd } },
	{ MODKEY,                       XK_e,      spawn,          {.v = emacs_cmd } },
	{ MODKEY,                       XK_Return, spawn,          {.v = term_cmd } },
	{ 0,                            XK_Print,  spawn,          {.v = flameshot_cmd } },

	/* Изменение громкости */
	{ 0,                            XF86XK_AudioRaiseVolume, spawn, volume_raise_cmd },
	{ 0,                            XF86XK_AudioLowerVolume, spawn, volume_lower_cmd },

	/* Изменение яркости экрана */
	{ 0,                            XF86XK_MonBrightnessUp, spawn, brightness_up_cmd },
	{ 0,                            XF86XK_MonBrightnessDown, spawn, brightness_dowm_cmd },

	{ MODKEY,                       XK_b,      togglebar,      {0} },
        
        /* Переключить фокус */
	{ MODKEY,                       XK_Tab,    focusstack,     {.i = +1 } },

	/* Перемещение окон в списке */
        
        /* Увеличить/уменьшить кол-во master окон */
	{ MODKEY,                       XK_bracketleft,  incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_bracketright, incnmaster,     {.i = -1 } },

        /* Для hjkl есть специальная функция, см. ниже (плавающие окна перемещаются, */
	/* hl для тайловых увеличивает/уменьшает ширину мастера, а jk перемещает окно по стеку */
	{ MODKEY,                       XK_h,      hjkl_func,      {.ui = 'h'} },
	{ MODKEY,                       XK_j,      hjkl_func,      {.ui = 'j' } },
	{ MODKEY,                       XK_k,      hjkl_func,      {.ui = 'k' } },
	{ MODKEY,                       XK_l,      hjkl_func,      {.ui = 'l'} },

	/* Ресайз плавающих окон */
	{ MODKEY|ShiftMask,             XK_j,      myresize,       {.v = "25v"} },
	{ MODKEY|ShiftMask,             XK_k,      myresize,       {.v = "-25v"} },
	{ MODKEY|ShiftMask,             XK_h,      myresize,       {.v = "-25h"} },
	{ MODKEY|ShiftMask,             XK_l,      myresize,       {.v = "+25h"} },
        
        /* Сделать окно мастером */
	{ MODKEY,                       XK_o,      zoom,           {0} },
        
        /* Закрыть окно */
	{ MODKEY,                       XK_q,      killclient,     {0} },

	/* Циклическое переключение режимов */
	{ MODKEY,           		XK_comma,  cyclelayout,    {.i = -1 } },

        /* Сделать текущее окно плавающим и переместить в центр экрана */
	{ MODKEY,                       XK_y,      float_and_center, {0} },

        /* Посмотреть все теги / присвоить все теги */
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },

        /* Mod+N переключиться к тегу */
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	TAGKEYS(                        XK_7,                      6)
	TAGKEYS(                        XK_8,                      7)
	TAGKEYS(                        XK_9,                      8)
        
        /* Выход из dwm */
	{ MODKEY|ShiftMask,             XK_q,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	/* { ClkLtSymbol,          0,              Button1,        setlayout,      {0} }, */
	/* { ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} }, */
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

void
hjkl_func(const Arg *arg) {
	Client *c;
	c = selmon->sel;
	char is_float = !(selmon->lt[selmon->sellt]->arrange && !c->isfloating);
	if (is_float) {
		if (arg->ui == 'j')
			mymove(c, 25, 'v');
		else if (arg->ui == 'k')
			mymove(c, -25, 'v');
		else if (arg->ui == 'l')
			mymove(c, 25, 'h');
		else if (arg->ui == 'h')
			mymove(c, -25, 'h');
	} else {
		if (arg->ui == 'j') {
			Arg a = {.i = +1};
			movestack(&a);
		} else if (arg->ui == 'k') {
			Arg a = {.i = -1};
			movestack(&a);
		} else if (arg->ui == 'l') {
			Arg a = {.f = +0.01};
			setmfact(&a);
		} else if (arg->ui == 'h') {
			Arg a = {.f = -0.01};
			setmfact(&a);
		}
	}
}

void
float_and_center(const Arg *arg) {
	Client *c;
	int nx, ny;
	int nw = 900, nh = 700;
	c = selmon->sel;

	togglefloating(arg);
	if (c->isfloating) {
		/* first resize */
		resize(c, 0, 0, nw - 2*c->bw, nh - 2*c->bw, 0);
		nx = selmon->wx + (selmon->ww / 2) - (c->w / 2);
		ny = selmon->wy + (selmon->wh / 2) - (c->h / 2);
		/* next move */
		resize(c, nx, ny, c->w, c->h, 0);
	}
}

void
mymove(Client *c, int step, char direction) {
	int hstep = 0, vstep = 0;

	if (selmon->lt[selmon->sellt]->arrange && !c->isfloating)
		return;

	if (direction == 'h')
		hstep = step;
	else
		vstep = step;

	resize(c, c->x + hstep, c->y + vstep, c->w, c->h, 1);
	change_cursor_position(c, 0, 0, 0, 0);
}

/* удобрый ресайз */
void
myresize(const Arg *arg) {
	Client *c;
	int step;
	int wstep, hstep;
	int wcur, hcur;
	char direction;

	c = selmon->sel;

	if (sscanf((char *)arg->v, "%d%c", &step, &direction) != 2)
		return;

	if (selmon->lt[selmon->sellt]->arrange && !c->isfloating)
		return;

	if (direction == 'h') {
		hstep = 0;
		wstep = step;
		if (c->incw)
			wstep -= wstep % c->incw;
	} else {
		hstep = step;
		wstep = 0;
		if (c->inch)
			hstep -= hstep % c->inch;
	}

	wcur = c->w;
	hcur = c->h;

	/* по стандарту если указаны basew и baseh aspect считается после вычитания */
	/* (см. ICCCM 4.1.2.3) */
	if (!(c->basew == c->minw && c->baseh == c->minh)) {
		wcur -= c->basew;
		hcur -= c->baseh;
	}

	/* если что-то не так с aspect ratio, постараемся развернуться в обе стороны */
	if (c->mina > 0 && c->maxa > 0) {
		if ((c->maxa < ((float) (wcur + wstep)) / (hcur + hstep)) ||
		    (c->mina < ((float) (hcur + hstep)) / (wcur + wstep))) {
			hstep = hstep == 0 ? wstep : hstep;
			wstep = hstep;
		}
	}

	/* возвращаем всё обратно */
	if (!(c->basew == c->minw && c->baseh == c->minh)) {
		wcur += c->basew;
		hcur += c->baseh;
	}

	resize(c, c->x, c->y, wcur + wstep, hcur + hstep, 1);

	/*  двигаем курсор, чтобы не заморачиваться, в центр окна */
	change_cursor_position(c, 0, 0, 0, 0);
}

void
change_cursor_position(Client *c, int oldx, int oldy, int oldw, int oldh) {
	int px, py, di;
	unsigned int dui;
	Window dw;
	Bool xqp;

	xqp = XQueryPointer(dpy, root, &dw, &dw, &px, &py, &di, &di, &dui);
	if (xqp)
		XWarpPointer(dpy, None, None, 0, 0, 0, 0,
			     c->x + c->w / 2 - px,
			     c->y + c->h / 2 - py);
}
